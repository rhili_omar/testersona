<?php
// Add code for logging out (session unset/destroy)
// Redirect to login.html after logout
header("Location: ../login.html");
exit();
?>
